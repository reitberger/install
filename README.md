## Make is great ... overuse it
### Usage: clone this repo with `git clone https://gitlab.com/reitberger/install`
### execute make commands
 Options:
- make all => runs `dotclean && dotfiles && zsh_dept && vim_dept && vim_packer`
- make dotfiles => clone dotfiles to `$PWD/dotfiles` and put them in `$HOME/.config`
- make dotclean => remove dotfiles from `$HOME/.config`
- make zsh_dep => download zsh dependencies using `pacman` // requires root
- make vim_dep => download nvim dependencies using `pacman` // requires root
- make vim_packer => download `github.com/wbthomason/packer.nvim`, start vim and perform `PackerSync` // will fail if packer is already installed
- make clean => remove cloned dotfiles from `$PWD/dotfiles`
