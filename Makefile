GCLONE=git clone
GITURL=https://gitlab.com/reitberger
CONFIG= $(HOME)/.config
$(CONFIG):
	mkdir $(CONFIG)
all: 
	make dotclean && make dotfiles && make zsh_dep && make vim_dep && make vim_packer
dotfiles: $(CONFIG)
	rm -rf $@
	mkdir -p $@
	$(GCLONE) $(GITURL)/dotfiles $@
	cp -ifr $@/* $(CONFIG)
	cp -ifr $@/.git $@/.gitignore $(CONFIG)
dotclean:
	rm -rf $(CONFIG)/.git
	rm -rf $(CONFIG)/.gitignore
	rm -rf $(CONFIG)/alacritty
	rm -rf $(CONFIG)/nvim
	rm -rf $(CONFIG)/zsh
	rm -rf $(CONFIG)/i3
	rm -rf $(CONFIG)/i3status
zsh_dep:
	sudo pacman -S zsh-syntax-highlighting zsh-autosuggestions zsh-history-substring-search zsh-theme-powerlevel10k
vim_dep:
	sudo pacman -S vim-spell-de
vim_packer:
	git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 		~/.local/share/nvim/site/pack/packer/start/packer.nvim
	nvim +PackerSync
clean:
	rm -rf dotfiles
